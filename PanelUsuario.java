import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

class PanelUsuario extends JPanel {

		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private JTextField userInput;
		private JButton okButton;

		public PanelUsuario() {
			this.setLayout(new GridLayout(1, 2));
			userInput = new JTextField();
			this.add(userInput);
			okButton = new JButton("OK");
			this.add(okButton);
		}

		public JTextField getUserInput() {
			return userInput;
		}

		public JButton getOkButton() {
			return okButton;
		}

	}