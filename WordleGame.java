import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.Toolkit;
import java.awt.Color;
import java.awt.Font;

public class WordleGame implements ActionListener {

	private JFrame gameFrame;
	private PanelPalabras[] wordPanelArray = new PanelPalabras[6];
	private PanelUsuario userPanel;
	private String wordleString;
	private int count = 0;

	public WordleGame() {
		gameFrame = new JFrame("Wordle Game");
		gameFrame.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\armin\\Documents\\ungs\\programacion 3\\progra3.2\\Eclipse\\wordle\\Tpractico\\src\\Imagen\\wordle.png"));
		gameFrame.setSize(600 , 600);
		gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gameFrame.getContentPane().setLayout(new GridLayout(7, 1));
		gameFrame.setVisible(true);
		gameFrame.setLocationRelativeTo(null);

		for (int i = 0; i < 6; i++) {
			wordPanelArray[i] = new PanelPalabras();
			gameFrame.getContentPane().add(wordPanelArray[i]);
		}
		userPanel = new PanelUsuario();
		userPanel.getOkButton().setFont(new Font("Tahoma", Font.PLAIN, 30));
		userPanel.getUserInput().setFont(new Font("Times New Roman", Font.PLAIN, 30));
		userPanel.getOkButton().addActionListener(this);
		gameFrame.getContentPane().add(userPanel);
		gameFrame.revalidate();
		
		wordleString = this.getWordleString();
		
		System.out.println("Word for the day : " + wordleString);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String userWord = this.userPanel.getUserInput().getText();
		
		if (userWord.length() >= 4) {
			if (isWordleWordEqualTo(userWord.trim().toUpperCase())) {
				
				JOptionPane.showMessageDialog(null, "You Win!!!", "Congrats", JOptionPane.INFORMATION_MESSAGE);
				gameFrame.dispose();
				return;
			}
			
		}
		if (userWord.length() > 5 || userWord.length()<4 ) {
			JOptionPane.showMessageDialog(null, "ERROR mas de 5 letras","vuelva a ingresar otra palabra", JOptionPane.INFORMATION_MESSAGE);
			
		}
		if (count > 4) {
			JOptionPane.showMessageDialog(null, "You Lost.Better luck next time.", "Oops",
					JOptionPane.INFORMATION_MESSAGE);
			gameFrame.dispose();
			
			
			return;
		}
		count++;
	}
	
	public PanelPalabras getActivePanel() {
		return this.wordPanelArray[count];
	}
	
	private boolean isWordleWordEqualTo(String userWord) {
		List<String> wordleWordsList = Arrays.asList(wordleString.split(""));
		String[] userWordsArray = userWord.split("");
		List<Boolean> wordMatchesList = new ArrayList<>();

		for (int i = 0; i < 5; i++) {
			if (wordleWordsList.contains(userWordsArray[i])) {
				if (wordleWordsList.get(i).equals(userWordsArray[i])) {
					getActivePanel().setPanelText(userWordsArray[i], i, Color.GREEN);
					wordMatchesList.add(true);
				} else {
					getActivePanel().setPanelText(userWordsArray[i], i, Color.YELLOW);
					wordMatchesList.add(false);
				}
			} else {
				getActivePanel().setPanelText(userWordsArray[i], i, Color.GRAY);
				wordMatchesList.add(false);
			}
		}
		return !wordMatchesList.contains(false);
	}

	

	public String getWordleString() {
		
		FileReader wordFile;
		BufferedReader word;
		List<String> wordList = null;
		
		try {
			wordFile = new FileReader("C:\\\\Users\\\\armin\\\\Documents\\\\ungs\\\\programacion 3\\\\progra3.2\\\\Eclipse\\\\wordle\\\\Tpractico\\\\src\\\\Imagen\\\\Words.txt");
			word = new BufferedReader(wordFile);
			String palabra;
			wordList = new ArrayList<>();
			 while ((palabra = word.readLine() )!=null){
					wordList.add(palabra);
				
			} 
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 
		 Random random = new Random();
		int position = random.nextInt(wordList.size());
		return wordList.get(position).trim().toUpperCase();
	
	}


	



}

