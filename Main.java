import java.awt.EventQueue;


public class Main {

	
	/**
	 * EventQueue.invokeLater(new Runnable(). El procesamiento completo de Swing se realiza en un hilo llamado EDT (Event Dispatching Thread). Por lo que de la forma tradiconal podr�a bloquear este hilo, ya que tu programa implicitamente lo est� usando (con Swing).
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Inicio window = new Inicio();
					window.setVisible(true);
					window.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
}
