import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

class PanelPalabras extends JPanel {

		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		JLabel[] wordColumns = new JLabel[5];

		public PanelPalabras() {
			setBackground(Color.LIGHT_GRAY);
			this.setLayout(new GridLayout(1, 5));
			Border blackBorder = BorderFactory.createLineBorder(Color.LIGHT_GRAY);
			for (int i = 0; i < 5; i++) {
				wordColumns[i] = new JLabel();
				wordColumns[i].setHorizontalAlignment(JLabel.CENTER);
				wordColumns[i].setOpaque(true);
				wordColumns[i].setBorder(blackBorder);
				this.add(wordColumns[i]);
			}
		}

		
		// este metodo permite sabiendo la posicion poner la letra y cambiar de color el fondo de 
		//de cada cuadrito
		public void setPanelText(String charValue, int position, Color color) {
			this.wordColumns[position].setText(charValue);
			this.wordColumns[position].setBackground(color);
		}
	}